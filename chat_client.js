var chatMsg = function(msg){
	this.level = msg.level;
	this.from = msg.from;
	this.content = msg.content;
	this.print = this.from+' : '+this.content;
}

var chatApp = {
	user: {
		default_room: true
	},
	connected: false,
	socket: io(),
	run: function(e){
		e.append(this.roomList.render());
		e.append(this.chat.render());
		e.append(this.userList.render());
		$('.input-form button').click(this.chat.sendMessage);
		this.socket.on('chat-message', function(data){
			chatApp.chat.printMessage(data);
		});
		this.socket.on('room-users', function(data){
			chatApp.userList.refresh(data);
		});
		this.socket.on('room-list', function(data){
			chatApp.roomList.refresh(data);
		});
	},
	connect: function(data){
		this.user.pseudo = data;
      	this.socket.emit('connection-login', this.user.pseudo);
	},
	chat: {
		render: function(){
			return '<div class="main-content"><div class="messages"></div><div class="input-form"><div class="input-group"><input type="text" class="form-control" placeholder="Your message here ..."><span class="input-group-btn"><button type="submit" class="btn btn-primary" data-type="last">Send</button></span></div></div></div>'
		},
		reset: function(){
			$('.messages').html('');
		},
		printMessage: function(msg){
			$(".messages").append(this.prettyMsg(msg));
			$(".messages .well").last().get(0).scrollIntoView();
		},
		prettyMsg: function(msg){
			var res = '<div class="well fadeInLeft animated">';
			res += '<div class="from">'+msg.from+'</div>';
			res += '<div class="content">'+msg.content+'</div>';
			res += "</div>";
			return res;
		},
		sendMessage: function(){
			var msg = {
				level: 0,
				from: chatApp.user.pseudo,
				content: $('.input-form input[type=text]').val()
			};
			chatApp.socket.emit('chat-message', msg);
			chatApp.chat.printMessage(msg);
		}
	},
	roomList: {
		rooms: [],
		render: function(){
			var res = '<div class="room-list"><h3>Rooms</h3><ul>';
			
			this.rooms.forEach(function(item){
				res += '<li>'+item+'</li>';
			});
			return res+'</ul></div>';
		},
		refresh: function(data){
			this.rooms = data;
			var res = '';

			$('.room-list ul').html('');
			this.rooms.forEach(function(item){
				$('.room-list ul').append(
					$('<li>'+item+'</li>').click(function(){chatApp.roomList.changeRoom(this)})
				);
			});
		},
		changeRoom: function(new_room){
			console.log(new_room);
			$(new_room).addClass("active");
			$(new_room).siblings().removeClass("active");
			chatApp.chat.reset();
			chatApp.socket.emit('change-room', $(new_room).html())
		}
	},
	userList: {
		users: [],
		render: function(){
			var res = '<div class="users-list"><h3>Users</h3><ul>';
			this.users.forEach(function(item){
				res += '<li>'+item.pseudo+'</li>';
			});
			return res+'</ul></div>';
		},
		refresh: function(data){
			this.users = data;
			var res = '';

			$('.users-list ul').html('');

			this.users.forEach(function(item){
				$('.users-list ul').append(
					$('<li>'+item.pseudo+'</li>').click(function(){chatApp.userList.talkTo(item)})
				);
			});

			//$('.users-list ul').html(res);
		},
		talkTo: function(user){
			console.log('talking to '+user);
		}
	}

}

/*var connected = false;
var printMessage = function(message){
	$("#messages").append("<li>"+message.print+"</li>");
}
var 
$('form').submit(function(){
	if(connected){
		var msg = {
			level: 0,
			from: user.pseudo,
			content: $('#m').val()
		};
		socket.emit('chat-message', msg);
		printMessage(new chatMsg(msg));
	}
	else{
		user.pseudo = $('#m').val();
		socket.emit('connection-login', user.pseudo);
		$('#m').attr("placeholder", "votre message ...");
		connected = true;
	}
	$('#m').val('');
	return false;
});

socket.on('chat-message', function(data){
	printMessage(new chatMsg(data));
})*/