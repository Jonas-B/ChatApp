var chat = {
	rooms: {
		HOME : [],
		WORK: [],
		HOOBIES : []
	},
	messages: {
		welcome: function(user){
			return {
				level: 1,
				user : 'Welcome to the '+user.room+' room, '+user.pseudo+' !',
				room: user.pseudo+' has join the '+user.room+' room'
			}
		},
		quit: function(user){
			return {
				level: 1,
				room: user.pseudo+' has quit the '+user.room+' room. Connected for '+((new Date().getTime() - user.startTime)/1000)+'s'
			}
		}
	},

    id: 0,
    nbUsers: function(room){
    	if(room != undefined){
    		return chat.rooms[room].length;
    	}
    	else{
    		var nb = 0;
    		for(room in chat.room){
    			nb += chat.nbUsers(room);
    		}
    		return nb;
    	}
    },
	run: function(io){
		io.on('connection', function(socket){
			socket.on('connection-login', function(login){
				var user = {
					id: chat.id++,
					pseudo : login,
					room : Object.keys(chat.rooms)[0],
					socket : socket,
					startTime : new Date().getTime()
				};
				console.log(user);

				user.socket.emit('room-list', Object.keys(chat.rooms));

				chat.addUser(user);

			    socket.on('change-room', function(new_room){
			    	chat.changeUser(user, new_room);
				});

				socket.on('chat-message', function(message){
					chat.roomMsg(user, message);
				})

			    socket.on('disconnect', function(){
				    chat.removeUser(user);
				});
			});
		});
	},
	addUser: function(user){
		chat.welcomeMsg(user);
		chat.rooms[user.room].push(user);
		chat.userListMsg(user);
	},
	changeUser: function(user, room){
		chat.removeUser(user, function(){
			user.room = room;
			chat.addUser(user);
		});
	},
	removeUser: function(user, then){
		var msg = chat.messages.quit(user);
		var tmp_msg = {
			level: msg.level,
			from: 'Server',
			content: msg.room
		}
		console.log(msg.room);
	    var i = 0;
	    chat.rooms[user.room].forEach(function(item){
    		if(item.id === user.id){
			    chat.rooms[user.room].splice(i, 1);
			    chat.serverMsg([user.room], tmp_msg);
			    chat.userListMsg(user);
			    if(then != undefined){then();}
			    return;
			}
			i++;
		});
	},
	welcomeMsg: function(user){
		chat.serverAsynchronousMsg(user, chat.messages.welcome(user));
	    console.log(user.pseudo+' has join the '+user.room+' room');
	},
	userListMsg: function(user){
		var anonymous = 0;
		var users = chat.rooms[user.room].filter(function(i){
			if(i.pseudo === 'anonymous'){
				anonymous++;
				return false;
			}
			else{
		    	return true;
		    }
	    });
		users = users.map(function(i){
			return {
				id: i.id,
				pseudo: i.pseudo
			}
		})
	    if(anonymous > 0){
	    	users.push({id: -1, pseudo: '+'+anonymous+' anonymous'});
	    }

		chat.rooms[user.room].forEach(function(item){
			item.socket.emit('room-users', users);
		})
	},
	serverAsynchronousMsg: function(user, msg){
		var tmp_msg = {
			level: msg.level,
			from: 'Server',
			content: msg.user
		}
		chat.simpleMsg(user.socket, tmp_msg);
		tmp_msg.content = msg.room;
	    chat.serverMsg([user.room], tmp_msg);
	},
	serverMsg: function(rooms, msg){
		rooms.forEach(function(room){
			chat.rooms[room].forEach(function(item){
				chat.simpleMsg(item.socket, msg);
			});
		})
	},
	roomMsg: function(user, msg){
		chat.rooms[user.room].forEach(function(item){
			if(item.id != user.id){
				chat.simpleMsg(item.socket, msg);
			}
		});
	},
	simpleMsg: function(socket, msg){
		socket.emit('chat-message', msg);
	}
}

module.exports = chat;