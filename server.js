var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// HTTP Client
app.get('/', function(req, res){
  res.sendFile(__dirname + '/client.html');
});

// Initialisation des salles
var rooms = {
	home : [],
	work: [],
	hoobies : []
}

/*var welcomeMsg = function(room, user){
	rooms[room].forEach(function(i, item){
		item.socket.emit('chat-msg', {
			level: 1,
			from: 'Server',
			content: e.pseudo+' has join the '+e.room+' room'
		})
	});
}*/

var id = 0;
// Socket connexion
io.on('connection', function(socket){
	socket.on('connection-login', function(login){
		var user = {
			id: id++,
			pseudo : login,
			room : 'home',
			socket : socket,
			startTime : new Date().getTime()
		};

		rooms[user.room].forEach(function(item){
			item.socket.emit('chat-message', {
				level: 1,
				from: 'Server',
				content: user.pseudo+' has join the '+user.room+' room'
			})
		});
		rooms.home.push(user);
	    console.log(user.pseudo+' has join');

	    socket.emit('room-users', rooms['home'].map(function(i){return i.pseudo}));
	    socket.emit('chat-message', {
	    	level: 1,
	    	from: 'Server',
	    	content: 'Welcome to the '+user.room+' room, '+user.pseudo+' !'
	    });

	    socket.on('change-room', function(new_room){
	    	var i = 0
	    	rooms[user.room].forEach(function(item){
	    		if(item.id === user.id){
	    			rooms[user.room].splice(i, 1);
	    			user.room = new_room;
	    			welcomeMsg();
				    rooms[new_room].push(user);

				    socket.emit('chat-message', {
				    	level: 1,
				    	from: 'Server',
				    	content: 'Welcome to the '+user.room+' room, '+user.pseudo+' !'
				    });

				    rooms[user.room].forEach(function(item){
						item.socket.emit('chat-message', {
							level: 1,
							from: 'Server',
							content: user.pseudo+' has joined the '+user.room+' room'
						})
					});
	    		}
	    		i++;
	    	})
		});

		socket.on('chat-message', function(message){
			rooms[user.room].forEach(function(item){
				if(item.id != user.id){
					item.socket.emit('chat-message', message);
				}
			})
		})

	    socket.on('disconnect', function(){
		    console.log(user.pseudo+' has quit');
		    var i = 0
		    rooms[user.room].forEach(function(item){
	    		if(item.id === user.id){
				    rooms[user.room].splice(i, 1);
				    rooms[user.room].forEach(function(item){
						item.socket.emit('chat-message', {
							level: 1,
							from: 'Server',
							content : user.pseudo+' has quit. Connected for '+((new Date().getTime() - user.startTime)/1000)+'s'
						});
					})
				}
				i++;
			});
		});
	});
});

// HTTP Server
http.listen(3000, function(){
  console.log('listening on *:3000');
});